
var dcolor = "rgba(0, 255, 0, 0.7)";
var fillcolor = "#66B032";
    var itextsize = 18;
    var shape_start = 160;
var itextbold = 'normal';
var itextstyle = 'normal';
var itextdecor = '';
var firstname = "Guest" 

var class_settings = function () {}
var whiteboard_settings = function () {
    var dhex = "#66B032";
    var json = false;
    var currentDocument = false;
    var doctype = false;
    var pagenum = 1;
    var erase = false;
    var SINGLE_LINE = false;
    var ntext1 = false;
    var resizeId = 0;
    var ratio = 1;
}

var text_settings = function () {
    var highlight = false;
}

var psettings = {
    size : 80,
    spd : 300,
    color : "#a00"
}
var circle_style = {
    "position":"absolute",
    "z-index":9999,
    "height":10,
    "width":10,
    "border":"solid 4px "+psettings.color,
    "border-radius":psettings.size
}



$("#startloader").fadeIn(); 


function gotopage(pagenumber) {
    whiteboard_settings.pagenum = $('.go-to-page input').val();
    if(whiteboard_settings.pagenum < 1){
        whiteboard_settings.pagenum == 1;
    }
    $('.go-to-page input').val(pagenumber);
    $('.go-to-page input').trigger("change");
}

$(document).on('keyup', '.go-to-page input', function(e) {
    whiteboard_settings.pagenum = $('.go-to-page input').val();
    if(whiteboard_settings.pagenum < 1){
        whiteboard_settings.pagenum == 1;
    }
    new SendPage(whiteboard_settings.pagenum);
    $('.go-to-page input').val(parseInt(whiteboard_settings.pagenum));
});

$(document).on('click', '.next-page', function(e) {
   if(whiteboard_settings.pagenum < 1){
    whiteboard_settings.pagenum == 1;
}
$('.go-to-page input').val(parseInt(whiteboard_settings.pagenum));
});

$(document).on('click', ".prev-page", function(e) {
    whiteboard_settings.pagenum = $('.go-to-page input').val();
    whiteboard_settings.pagenum = whiteboard_settings.pagenum - 1;
    if(whiteboard_settings.pagenum < 1){
        whiteboard_settings.pagenum == 1;
    }
    $('.go-to-page input').val(parseInt(whiteboard_settings.pagenum));
});


whiteboard_settings.currentDocument, currentWidth = 880 //$("#main").width()-20,

$sidebar = $('#sidebar'),
        //initialize the plugin
        myDocViewer = $('#document-preview').documentViewer({
         path: 'https://static.learncube.net/js/documentViewer/',
         autoLoadDependencies: false,
         width: currentWidth
            //debug:true
        });


    function doc_filter_send(currentDocument, doctype, load) {
       whiteboard_settings.currentDocument = currentDocument;
       if (doctype !== "image") {
        if (load) {
            canvas.clear();
        }
    }
    if (load) {
        get_doc(whiteboard_settings.currentDocument, doctype);
    }
}


$(document).ajaxComplete(function() {
        // fire when any Ajax requests complete
        //initialize the  file extension icons (in the sidebar)
        $('.document-list').find('div').each(function(i, link) {

            var $link = $(link),
            href = $link.attr('data-path'),
            ext = /(?:\.([^.]+))?$/.exec(href)[1];

            //since the text and code files need relative paths, we do not want to append the base path
            if (!$link.hasClass('text-code'))
                $link.attr('data-path');

            //populate the file type icon
            if (!ext) {
                ext = '<i class="fa fa-link"></i>';
            }

            $link.find('.ext').html(ext);
        });


        //handle document clicks in the sidebar
        $('.document-list').find('div').on('click', function(e) {
            var $document = $(this);
            //mark this document as active/highlighted
            $sidebar.find('.active').removeClass('active');
            $document.parent().addClass('active');
            whiteboard_settings.currentDocument = $document.attr('data-path');
            whiteboard_settings.doctype = $document.attr('data-type');
            //load the document
            var load = true;
            doc_filter_send(whiteboard_settings.currentDocument, whiteboard_settings.doctype, load);
            e.preventDefault();
        });


        $('#id_content_type').on('change', function() {
            var content_type = $("#id_content_type").val();
            if (content_type == "embedlink" || content_type == "youtube") {
                $("#file-upload-input").hide();
                $("#embed-input").fadeIn();
                $("#upload-inputs").fadeIn();
                $("#id_embed_link").prop('required', true);
                $(".file-input").prop('required', false);

            } else {
                $("#id_embed_link").prop('required', false);
                $(".file-input").prop('required', true);
                $("#embed-input").hide();
                $("#file-upload-input").fadeIn();
                $("#upload-inputs").fadeIn();
            }
        });

    });
    //ajax complete

    function get_doc(currentDocument, doctype) {
      $(".document-viewer-wrapper").show();
      $(".canvas-container").show();
      $("#whiteboard-embedd").hide();
      $("#youtube-embedd").hide()
      if (doctype !== "image") {
          whiteboard_settings.currentDocument = currentDocument;
          whiteboard_settings.doctype = doctype;
      }

      if (doctype == "audio") {

        canvas.clear();
        currentAudio = currentDocument;
        if ($('#audio-load .document-viewer-wrapper').length) {
            AudioViewer.load(currentAudio, {
                width: currentWidth,
                autoplay: false,
            });
            $("#audio-load").fadeIn();
    
        }

    } else if (doctype == "image") {
        fabric.Image.fromURL(currentDocument, function(oImg) {
            oImg.set({
                id: userid + Math.random().toString(16).slice(2),
                crossOrigin: 'anonymous',
                top: 200,
                left: 200,
            });
            canvas.add(oImg);
            create_json(oImg);
        });

    } else if (doctype == "embedlink") {
        canvas.clear();
        $(".document-viewer-wrapper").hide();
        $(".canvas-container").hide();
        $("#whiteboard-embedd").show();
        $("#whiteboard-embedd").attr("src", currentDocument);



  } else {
    canvas.clear();

    myDocViewer.load(currentDocument, {
        width: currentWidth,
        callback: function() {
            $("#sidebar").hide('slide', {
                direction: 'right'
            }, 400);
            canvas_resize();
            $('.go-to-page input').val(parseInt(1));
        }
    });

}

}

$(document).on('show', '.accordion', function(e) {
    $(e.target).prev('.accordion-heading').addClass('accordion-opened');
});

$(document).on('hide', '.accordion', function(e) {
    $(this).find('.accordion-heading').not($(e.target)).removeClass('accordion-opened');
});


$(document).on('click', '#btn-upload', function() {
   $("#file-search").hide();
   $("#btn-upload").hide();
   $("#file-list-inner").remove();
   $("#file-upload").load("/files/upload/?userid=" + userid);
});

/* Canvas */

$("#colorinner").spectrum({
    color: dcolor,
    showAlpha: true,
    clickoutFiresChange: true,
    cancelText: " ",
    preferredFormat: "hex",
    move: function(color) {
        change_color(color.toRgbString());
    }

});

function change_color(color) {
    whiteboard_settings.dhex = color;
    dcolor = color;
    if (canvas.isDrawingMode) {
        canvas.isDrawingMode = true;
        canvas.freeDrawingBrush.color = color;
    }

    var activeObject = canvas.getActiveObject();

    try {
        if (activeObject.fill == "transparent") {
            fillcolor = "transparent";
        } else {
            fillcolor = color;
        }
        activeObject.fill = color;
    } catch (err) {
        console.log("object has no fill property" + err);
    }

    try {
        activeObject.stroke = color;
    } catch (err) {
        console.log("object has no stroke property" + err);
    }

    var obj = activeObject;
    if (obj) {
        var obj_fill = obj.get('fill');
        var obj_stroke = obj.get('stroke');
        send_update_color(obj.id, obj_fill, obj_stroke);
    }

    try {
        if (obj.isSameColor && obj.isSameColor() || !obj.paths) {
          obj.setFill(color);
          obj.setStroke(color);
      }
      else if (obj.paths) {
          for (var i = 0; i < obj.paths.length; i++) {
            obj.paths[i].setFill(dcolor);
            obj.paths[i].setStroke(dcolor);
        }
    }
} catch (err) {
    console.log(err);
}
canvas.renderAll();
}


$( window ).resize(function() {
    clearTimeout(whiteboard_settings.resizeId);
    whiteboard_settings.resizeId = setTimeout(canvas_resize, 500);
});


function canvas_resize(factor, json) {

    srcWidth=880;
    srcHeight=1144;

        maxWidth = $("#main").width();

    maxHeight = maxWidth / (1/1.4142)

    whiteboard_settings.ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    currentWidth = srcWidth*whiteboard_settings.ratio;
    currentHeight = srcHeight*whiteboard_settings.ratio;


    canvas.setWidth(currentWidth);
    canvas.setHeight(currentHeight);
    canvas.setZoom(whiteboard_settings.ratio);
    canvas.calcOffset();
    canvas.renderAll();

    $(".document-viewer").width(currentWidth).height(currentHeight);
        var cpad = (maxWidth-currentWidth)/2
        $(".canvas-container").width(currentWidth).height(currentHeight).css("left",cpad-5);
        $(".document-viewer-wrapper").width(currentWidth).css("left",cpad);

    $("#youtube-embedd").width(currentWidth);

    $(".document-viewer-wrapper").width(currentWidth)
    $(".document-viewer-outer").width(currentWidth).height(currentHeight); 
    $(".document-viewer canvas").css('margin-top',(30*whiteboard_settings.ratio + 'px')); 


    z_width = ($("#document-preview .document-viewer-wrapper").width()) + 30;
    z_height = ($("#document-preview .document-viewer-wrapper").height()) + 30;

        canvas.setZoom(canvas.getZoom()*1);


            $(".document-viewer").width($(".document-viewer").width()*1);
            $(".document-viewer").height($(".document-viewer").width()* (1/1.4142));

            $(".document-viewer-outer").width($(".document-viewer-outer").width()*1);
            $(".document-viewer-outer").height($(".document-viewer-outer").width()* (1/1.4142));

            $(".document-viewer-wrapper").width($(".document-wrapper").width()*1);
            $(".document-viewer-wrapper").height($(".document-wrapper").width()* (1/1.4142));
            $(".pdf-menu").css('left',$(".document-viewer-wrapper").css('left'));
    
        


    $(".jp-interface ").width(z_width - 60);
    $(".go-to-page input").val();
    $('.go-to-page input').trigger("change");

}


var canvas = new fabric.Canvas('drawCanvas');
canvas.setWidth(1654);
canvas.setHeight(2339);
canvas_resize();



canvas.selection = false;

function rectangle() {
    var rect = new fabric.Rect({
        id: userid + Math.random().toString(16).slice(2),
        user: firstname,
        left: 100,
        top: 100,
        fill: 'transparent',
        stroke: dcolor,
        width: 75,
        height: 75,
        strokeWidth: 4
    });

    canvas.add(rect);
        create_json(rect); //sync
    }

    function rectangle_fill() {
        var rect = new fabric.Rect({
            id: userid + Math.random().toString(16).slice(2),
            user: firstname,
            left: 100,
            top: 100,
            fill: fillcolor,
            stroke: dcolor,
            width: 65,
            height: 65
        });
        canvas.add(rect);
        create_json(rect); //sync
    }


    function ellipse() {
        var ellipse = new fabric.Ellipse({
            id: userid + Math.random().toString(16).slice(2),
            user: firstname,
            top: 100,
            left: 100,
            rx: 75,
            ry: 75,
            fill: 'transparent',
            stroke: dcolor,
            strokeWidth: 4
        });
        canvas.add(ellipse);
        create_json(ellipse); //sync
    }

    function ellipse_fill() {
        var ellipse = new fabric.Ellipse({
            id: userid + Math.random().toString(16).slice(2),
            user: firstname,
            top: 100,
            left: 100,
            rx: 65,
            ry: 65,
            fill: fillcolor,
            strokeWidth: 2
        });
        canvas.add(ellipse);
        create_json(ellipse); //sync
    }

    function text_obj() {
        var myText = new fabric.IText("Enter Answer", {
            id: 1 + Math.random().toString(16).slice(2),
            user: "Guest",
            top: shape_start * whiteboard_settings.ratio,
            left: shape_start * whiteboard_settings.ratio,
            height: 100,
            fill: fillcolor,
            fontSize: itextsize,
            fontWeight: itextbold,
            fontStyle: itextstyle,
            textDecoration: itextdecor,
            fontFamily: 'helvetica',
            padding: 20,
        });
        myText.set('showplaceholder', true);
        canvas.setActiveObject(myText);
        canvas.add(myText);
        var movedObject = myText;
        whiteboard_settings.ntext1 = true

        canvas.on({'mouse:up': function(e) {
            var newobj=canvas.getActiveObject();
            if(newobj){
                if(newobj.text==="Enter text" ){
                    newobj.enterEditing();
                }    
            }

        },'mouse:down': function(e) {
            //If an object is selected already it'll be unselected
            if (movedObject !== null) {
             canvas.setActiveObject(movedObject);
             movedObject.enterEditing();
             movedObject = null;
         }
         canvas.__eventListeners["mouse:move"] = [];
         canvas.__eventListeners["mouse:down"] = [];
         canvas.__eventListeners["mouse:up"] = [];
     },
     'mouse:move': function(e) {
    //Moving the object along with mouse cursor
    if (movedObject !== null) {
        mleft = $("#right-panel").width();
        movedObject.left = (e.e.clientX-(mleft*whiteboard_settings.ratio));
        movedObject.top = (e.e.clientY-(100*whiteboard_settings.ratio));
        movedObject.setCoords();
        canvas.renderAll();
    }
}
});
    }

    //arrow

    function addArrowToCanvas() {
        var svg_arrow='<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="213" height="71" viewBox="-0.709 -0.235 213 71" enable-background="new -0.709 -0.235 213 71" xml:space="preserve"><polygon points="0 26.488 0 44.144 167.747 44.144 167.747 70.631 211.89 35.316 167.747 0 167.747 26.488 "></polygon></svg>';

        fabric.loadSVGFromString(svg_arrow, function(objects, options) {
            var arrow = new fabric.util.groupSVGElements(objects, options);
            arrow.set({
               id: userid + Math.random().toString(16).slice(2),
               user: firstname,
               top: 20,
               left: 20,
           })

            if (arrow.isSameColor && arrow.isSameColor() || !arrow.paths) {
              arrow.setFill(dcolor);
              arrow.setStroke(dcolor);
          }
          else if (arrow.paths) {
              for (var i = 0; i < arrow.paths.length; i++) {
                arrow.paths[i].setFill(dcolor);
                arrow.paths[i].setStroke(dcolor);
            }
        }

        canvas.add(arrow);
        canvas.renderAll();
        create_json(arrow); //sync
    });
    }

    //delete
    function delete_obj() {
        var obj = canvas.getActiveObject();
        if(obj){
            canvas.remove(obj);
            send_remove_obj(obj.id);
            canvas.renderAll();
        }
    }



    //toggle erase mode
    function erase_obj(val) {
     whiteboard_settings.erase = true;
     canvas.isDrawingMode = val;
     canvas.freeDrawingBrush.color = "rgb(255, 255, 255)";
     canvas.freeDrawingBrush.width = 15;
     if (canvas.isDrawingMode && whiteboard_settings.erase) {
        $(".upper-canvas").addClass("show_eraser");
    } else {
        $(".upper-canvas").removeClass("show_eraser");
    }
}

    //toggle draw mode
    function draw_obj(val) {
        whiteboard_settings.erase = false;
        color = dcolor
        canvas.isDrawingMode = val;
        canvas.freeDrawingBrush.color = color;
        if(text_settings.highlight){
            canvas.freeDrawingBrush.width = 15;
            canvas.freeDrawingBrush.color = color.replace(/[^,]+(?=\))/, '0.3');
        }else{
            canvas.freeDrawingBrush.width = 4;
        }  
        if (canvas.isDrawingMode && !class_settings.erase) {
            $(".upper-canvas").addClass("show_pen");
        } else {
            $(".upper-canvas").removeClass("show_pen");
        }
        
        
    }

    function reset_tools() {
        canvas.deactivateAll().renderAll();
        canvas.isDrawingMode = false;
        $(".upper-canvas").removeClass("show_pen");
        $(".upper-canvas").removeClass("show_eraser");

    }

    canvas.observe('path:created', function(e) {
        var canvas_objects = canvas._objects;
        if (canvas_objects.length !== 0) {
            if(class_settings.erase){
                last_draw.hasControls = false;
                last_draw.selectable = false;
            }
            var last_draw = canvas_objects[canvas_objects.length - 1];
            last_draw.id = 1 + Math.random().toString(16).slice(2);
            last_draw.user = "Guest";
            create_draw(last_draw);
        }

    });



    canvas.on('mouse:out', function(e) {
     $('#imageDialog').hide();
     canvas.renderAll();
 });



    function getObjPosition(e) {
        // Get dimensions of object
        if (e) {
            var rect = e.getBoundingRect();
            // We have the bounding box for rect... Now to get the canvas position
            var offset = canvas.calcOffset();
            // Do the math - offset is from $(body)
            var bottom = offset._offset.top + rect.top + rect.height;
            var right = offset._offset.left + rect.left + rect.width;
            var left = offset._offset.left + rect.left;
            var top = offset._offset.top + rect.top;
            return {
                left: left,
                top: top,
                right: right,
                bottom: bottom
            };
        }
    }

    $("body").on("click", ".text-code", function() {
        $("#sidebar").hide('slide', {
            direction: 'right'
        }, 400);
        $("#mini-loader").remove();
    });

    canvas.observe('object:selected', function() {
        $(".wtop-menu").hide();
        var obj=canvas.getActiveObject();

        if(obj.text){
           $(".wtext").fadeIn();
       }else if(obj.stroke){
           $(".wdraw").fadeIn();
       } else{

        $(".wdelete").fadeIn();   
    }

    $("#mini-materials").addClass("t-pdf-menu");
    $('#imageDialog').hide();

    $(document).keydown(function(e) {
        if(obj){
            if(e.keyCode == 46 || e.keyCode == 8 ) {
              delete_obj();
          }
      }
  });
});

    canvas.observe('selection:cleared', function() {
        $("#mini-materials").removeClass("t-pdf-menu");
        $('#imageDialog').show();
    });

    // set up a listener for the event where the object has been modified
    canvas.observe('object:modified', function(e) {
        e.target.resizeToScale();
        var obj = e.target;
        var obj_left = obj.get('left');
        var obj_top = obj.get('top');
        var obj_width = obj.get('width');
        var obj_height = obj.get('height');
        var obj_angle = obj.get('angle');
        var obj_fill = obj.get('fill');
        var obj_stroke = obj.get('stroke');
        var obj_rx = obj.get('rx');
        var obj_ry = obj.get('ry');
        var obj_scalex = obj.get('scaleX');
        var obj_scaley = obj.get('scaleY');
        var obj_user = firstname;
        e.target.set({
            user: firstname
        });
        canvas.renderAll();
        //send_update_obj(obj.id, obj_left, obj_top, obj_width, obj_height, obj_angle, obj_fill, obj_stroke, obj_rx, obj_ry, obj_scalex, obj_scaley, obj_user);
        
    });

    // SEND ON COMPLETE

    canvas.on('text:changed', function(e) {
        var obj = e.target;

// Text now empty, show placeholder:
if(obj.getText() === '')
{
    obj.setText('Enter text');
    obj.set('opacity', 0.3);
    obj.set('showplaceholder', true); // Set flag on IText object
    obj.setCoords();
    canvas.renderAll();
}

// Placeholder currently active:
else if(obj.get('showplaceholder') === true)
{
  // The text in the IText should now be the placeholder plus the character that  was pressed, so text and placeholder should be different, so remove the placeholder (unless the pressed key was backspace in which case do nothing):
  if(e.target.text !== 'Enter text')
  {
    // New char should be at position 0, so remove placeholder from rest of text:
    obj.setText(obj.getText().substr(0,1));
    obj._updateTextarea();
    obj.set('opacity', 1);
        obj.set('showplaceholder', false); // Remove flag on IText object
        obj.setCoords();
        canvas.renderAll(); 
    }
}
});

    canvas.observe('text:editing:entered', function(e) {
        var obj = e.target;

        if (whiteboard_settings.SINGLE_LINE) {
            var $itext = $('<input/>').attr('type', 'text').addClass('itext');
        }
        else {
            var $itext = $('<textarea/>').addClass('itext');
        }

        if (whiteboard_settings.SINGLE_LINE) {
            var keyDownCode = 0;
        }

        if(obj.get('showplaceholder') == true && obj.text == "Text box")
        {
            obj.text="Enter text";
        // Move cursor to beginning of line:
        obj.setSelectionStart(0);
        obj.setSelectionEnd(0);
        canvas.renderAll();
    }
    canvas.remove(obj);
    tleft = obj.left * whiteboard_settings.ratio;
    ttop = (obj.top-10) * whiteboard_settings.ratio;
    // show input area
    $itext.css({
        position: 'absolute',
        left: tleft,
        top: ttop,
        padding: obj.padding,
        margin: obj.margin,
        'min-height': 100,
        'height': obj.height,
        'min-width': 300,
        'width': obj.width,
        'z-index': 999999999999999,
        'line-height': obj.lineHeight,
        'font-family': obj.fontFamily,
        'font-size': Math.floor(obj.fontSize * Math.min(obj.scaleX, obj.scaleY)) + 'px',
        'font-weight': obj.fontWeight,
        'font-style': obj.fontStyle,
        color: obj.fill
    })
    .appendTo($(canvas.wrapperEl).closest('.canvas-container'));


    // text submit event
    if (whiteboard_settings.SINGLE_LINE) {
        // submit text by ENTER key
        $itext.on('keydown', function(e) {
            // save the key code of a pressed key while kanji conversion (it differs from the code for keyup)
            keyDownCode = e.which;
        })
        .on('keyup', function(e) {
            if (e.keyCode == 13 && e.which == keyDownCode) {
                obj.exitEditing();
                obj.set('text', $(this).val());
                $(this).remove();
                canvas.add(obj);
                canvas.renderAll();
                obj.backgroundColor = "transparent";
            }
        });
    }
    else {

        // submit text by focusout
        $itext.on('focusout', function(e) {

            obj.exitEditing();
            obj.set('text', $(this).val());
            $(this).remove();
            obj.backgroundColor = "transparent";
            canvas.add(obj);
            canvas.renderAll();
            if(whiteboard_settings.ntext1){
                    create_json(obj); //sync
                    whiteboard_settings.ntext1 = false;
                }else{   
                   send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)

               }
           });
    }
    
    // focus on text
    setTimeout(function() {
        $itext.select();
    }, 1);
});



    // customise fabric.Object with a method to resize rather than just scale after tranformation
    fabric.Object.prototype.resizeToScale = function() {
        // resizes an object that has been scaled (e.g. by manipulating the handles), setting scale to 1 and recalculating bounding box where necessary
        switch (this.type) {
            case "circle":
            this.radius *= this.scaleX;
            this.scaleX = 1;
            this.scaleY = 1;
            break;
            case "ellipse":
            this.rx *= this.scaleX;
            this.ry *= this.scaleY;
            this.width = this.rx * 2;
            this.height = this.ry * 2;
            this.scaleX = 1;
            this.scaleY = 1;
            break;
            case "polygon":
            case "polyline":
            var points = this.get('points');
            for (var i = 0; i < points.length; i++) {
                var p = points[i];
                p.x *= this.scaleX;
                p.y *= this.scaleY;
            }
            this.scaleX = 1;
            this.scaleY = 1;
            this.width = this.getBoundingBox().width;
            this.height = this.getBoundingBox().height;
            break;
            case "triangle":
            case "line":
            case "rect":
            this.width *= this.scaleX;
            this.height *= this.scaleY;
            this.scaleX = 1;
            this.scaleY = 1;
            break;
            default:
        }
    };

    // helper function to return the boundaries of a polygon/polyline
    // something similar may be built in but it seemed easier to write my own than dig through the fabric.js code.  This may make me a bad person.
    fabric.Object.prototype.getBoundingBox = function() {
        var minX = null;
        var minY = null;
        var maxX = null;
        var maxY = null;
        switch (this.type) {
            case "polygon":
            case "polyline":
            var points = this.get('points');

            for (var i = 0; i < points.length; i++) {
                if (typeof(minX) === undefined) {
                    minX = points[i].x;
                } else if (points[i].x < minX) {
                    minX = points[i].x;
                }
                if (typeof(minY) === undefined) {
                    minY = points[i].y;
                } else if (points[i].y < minY) {
                    minY = points[i].y;
                }
                if (typeof(maxX) === undefined) {
                    maxX = points[i].x;
                } else if (points[i].x > maxX) {
                    maxX = points[i].x;
                }
                if (typeof(maxY) === undefined) {
                    maxY = points[i].y;
                } else if (points[i].y > maxY) {
                    maxY = points[i].y;
                }
            }
            break;
            default:
            minX = this.left;
            minY = this.top;
            maxX = this.left + this.width;
            maxY = this.top + this.height;
        }
        return {
            topLeft: new fabric.Point(minX, minY),
            bottomRight: new fabric.Point(maxX, maxY),
            width: maxX - minX,
            height: maxY - minY
        };
    };

    canvas.on('touch:shake',function(){
        clear_canvas();
    });

    canvas.on('touch:gesture',function(e, self){
        var re_enabeldraw=false;
        if(canvas.isDrawingMode==true){
            canvas.isDrawingMode=false;
            re_enabeldraw=true;
        }

        if (e.self.scale < 1.0 ) {
            if(canvas.getZoom()>0.2){
                zoom=canvas.getZoom();                
                canvas.setZoom(zoom / 1.2 ) ;


                $(".document-viewer").width($(".document-viewer").width()/1.2);
                $(".document-viewer").height($(".document-viewer").width()/ (1/1.4142));

                canvas.setHeight($(".document-viewer").width());
                canvas.setWidth($(".document-viewer").width());
                canvas.renderAll();


                $(".document-viewer-outer").width($(".document-viewer-outer").width()/1.2);
                $(".document-viewer-outer").height($(".document-viewer-outer").width()/ (1/1.4142));


                $(".document-viewer-wrapper").width($(".document-wrapper").width()/1.2);
                $(".document-viewer-wrapper").height($(".document-wrapper").width()/ (1/1.4142));

                $(".go-to-page input").val();
                $('.go-to-page input').trigger("change");
            }

        } else if (e.self.scale > 1.0) {

            if(canvas.getZoom()<3.2){

                zoom=canvas.getZoom(); 

                canvas.setZoom(zoom * 1.2 ) ;

                $(".document-viewer").width($(".document-viewer").width()*1.2);
                $(".document-viewer").height($(".document-viewer").width()/ (1/1.4142));

                canvas.setHeight($(".document-viewer").width());
                canvas.setWidth($(".document-viewer").width());
                canvas.renderAll();

                $(".document-viewer-outer").width($(".document-viewer-outer").width()*1.2);
                $(".document-viewer-outer").height($(".document-viewer-outer").width()/ (1/1.4142));

                $(".document-viewer-wrapper").width($(".document-wrapper").width()*1.2);
                $(".document-viewer-wrapper").height($(".document-wrapper").width()/ (1/1.4142));

                $(".go-to-page input").val();
                $('.go-to-page input').trigger("change");
            }
        }
        setTimeout(function() {
          if(re_enabeldraw==true){
            canvas.isDrawingMode=true;
        }

    }, 1000);    



    });

    //whiteboard buttons
    $("body").on("click", "#draw_obj", function() {
        $(".wtop-menu.wdraw").fadeIn();
        $("#mini-materials").addClass("t-pdf-menu");
        draw_obj(true);
    });

    $("body").on("click", "#text_obj", function() {
        $(this).addClass("active");
        $(".wtop-menu.wtext").fadeIn();
        $("#mini-materials").addClass("t-pdf-menu");
        reset_tools();
        text_obj();
    });


    $("body").on("click", "#arrow", function() {
        reset_tools();        
        addArrowToCanvas();
    });

    $("body").on("click", "#rectangle_fill", function() {
        reset_tools();
        rectangle_fill();
    });

    $("body").on("click", "#rectangle", function() {
        reset_tools();
        rectangle();
    });

    $("body").on("click", "#circle_fill", function() {
        reset_tools();
        ellipse_fill();
    });

    $("body").on("click", "#circle", function() {
        reset_tools();
        ellipse();
    });

    $("body").on("click", "#move", function() {
        draw_obj(false);
    });

    $("body").on("click", ".mdelete", function() {
        delete_obj(false);
    });

    
    $("body").on("click", "#blank", function() {
        var clear_conform = confirm(clearb);
        if (clear_conform === true) {
            clear_whiteboard();
        }
    });

    function clear_whiteboard() {
        whiteboard_settings.currentDocument = false;
        new_canvas();
        canvas_resize();
        $(".document-viewer-wrapper").show();
        $(".canvas-container").show();
        $("#whiteboard-embedd").hide();
        $("#youtube-embedd").hide();
        if($.isFunction(player.stopVideo)){
            player.stopVideo()
        }
        send_new_canvas();
    }

    function new_canvas() {
     whiteboard_settings.currentDocument = false;
     $("#youtube-embedd").hide();
     if($.isFunction(player.stopVideo)){
        player.stopVideo()
    }

    canvas.clear();
    if ($('#document-preview .document-viewer-wrapper').length) {
        myDocViewer.close();
    }
    if ($('#audio-load .document-viewer-wrapper').length) {
        AudioViewer.close();
    }
}

    //sync
    function create_json(obj) {
        var json_obj = JSON.stringify(obj);
    //no time
    }

    function unpack_add_obj(json_obj) {
        var parse_obj = JSON.parse(json_obj);
        fabric.util.enlivenObjects([parse_obj], function(objects) {
            var origRenderOnAddRemove = canvas.renderOnAddRemove;
            canvas.renderOnAddRemove = false;
            objects.forEach(function(o) {
                canvas.add(o);
            });
            canvas.renderOnAddRemove = origRenderOnAddRemove;
            canvas.renderAll();
        });
    }

    function create_draw(obj) {
     //no time
    }


    fabric.Canvas.prototype.getItemByID = function(id) {
        var object = null,
        objects = this.getObjects();

        for (var i = 0, len = this.size(); i < len; i++) {
            if (objects[i].id && objects[i].id === id) {
                object = objects[i];
                break;
            }
        }
        return object;
    };


    function update_color(obj_id, cfill, cstroke) {
        obj = canvas.getItemByID(obj_id);
        if (obj) {
            obj.set({
                fill: cfill,
                stroke: cstroke
            });

            canvas.renderAll();
        }
    }

    function update_text(obj_id, text, size, bold, style, decor) {
        obj = canvas.getItemByID(obj_id);
        if (obj) {
            obj.set({
                text: text,
                padding: 20,
                backgroundColor: "transparent",
                fontSize: size,
                fontWeight: bold,
                fontStyle: style,
                textDecoration: decor
            });

            canvas.renderAll();
        }
    }


    function update_obj(obj_id, cleft, ctop, cwidth, cheight, cangle, cfill, cstroke, crx, cry, cscalex, cscaley, cuser) {
        obj = canvas.getItemByID(obj_id);
        if (obj) {
            obj.set({
                left: parseInt(cleft),
                top: parseInt(ctop),
                width: parseInt(cwidth),
                height: parseInt(cheight),
                angle: parseInt(cangle),
                fill: cfill,
                stroke: cstroke,
                rx: parseInt(crx),
                ry: parseInt(cry),
                scaleX: cscalex,
                scaleY: cscaley,
                user: cuser
            });
            obj.setCoords();
            canvas.renderAll();
        }
    }


    function remove_obj(obj_id) {
        obj = canvas.getItemByID(obj_id);
        canvas.remove(obj);
    }


    //whiteboard tools
    $("body").on("click", ".navbar-toggler", function() {
    
            $(".navbar-minimal > .navbar-menu").slideDown();    
  
        $(".navbar-toggler").addClass("mclosed");
        $(".mbars").removeClass("fa-caret-square-o-up");
        $(".mbars").addClass("fa-bars");
        $(".wtop-menu").fadeOut();
        $(".wb-container").removeClass("wb-container-m");
        $("#sidebar").fadeOut();
        $(".pdf-menu").addClass("m-pdf-menu");
    });

    // toolbar
    $("body").on("click", ".navbar-toggler.mclosed", function() {

  
        $(".navbar-minimal > .navbar-menu").slideUp();    
 

    $(".navbar-toggler").removeClass("mclosed");

    $(".mbars").addClass("fa-chevron-up");
    $(".mbars").removeClass("fa-bars");
    $(".wtop-menu li a").fadeIn();
    $(".wtop-menu").fadeOut();
    $(".wb-container").addClass("wb-container-m");
    $(".pdf-menu").removeClass("m-pdf-menu");
    $(".navbar-menu animate").show();

});


    $("body").on("click", "#material-icon", function() {
        $("#file-upload-form").empty();
        $("#sidebar").show('slide', {
            direction: 'right'
        }, 400);
        $("#file-list").load("/files/?userid=" + userid);
        $("#btn-upload").show();
        $("#file-search").show();
    });

    $("#material-hide").click(function() {
        $("#sidebar").hide('slide', {
            direction: 'right'
        }, 400);
        $("#file-upload-form").empty();

    });

    $(".navbar-menu li a").click(function() {
      $(".wtop-menu").hide();
      $("#mini-materials").removeClass("t-pdf-menu");
      
  });


    $("body").on("click", "#shape", function() {
        $(".wtop-menu.wshape").fadeIn();
        $("#mini-materials").addClass("t-pdf-menu");
    });

    $("body").on("click", "#zoom", function() {
        $(".wtop-menu.zoom").fadeIn();
        $("#mini-materials").addClass("t-pdf-menu");
    });

    $("body").on("click", "#clear", function() {
        $(".wtop-menu.dela").fadeIn();
        $("#mini-materials").addClass("t-pdf-menu");
        reset_tools();
        erase_obj(true);
        
    });



    $("#delall").click(function() {
        clear_canvas();
    });

    $("#zoomin").click(function() {
        canvas.setZoom(canvas.getZoom() * 1.2 ) ;
        $(".document-viewer").width($(".document-viewer").width()*1.2)
        $(".document-viewer").height($(".document-viewer").width()/ (1/1.4142))

        $(".document-viewer-outer").width($(".document-viewer-outer").width()*1.2)
        $(".document-viewer-outer").height($(".document-viewer-outer").width()/ (1/1.4142))

        $(".document-viewer-wrapper").width($(".document-wrapper").width()*1.2)
        $(".document-viewer-wrapper").height($(".document-wrapper").width()/ (1/1.4142))

        $(".go-to-page input").val();
        $('.go-to-page input').trigger("change");
    });


    $("#zoomout").click(function() {
        canvas.setZoom(canvas.getZoom() / 1.2 ) ;
        $(".document-viewer").width($(".document-viewer").width()/1.2)
        $(".document-viewer").height($(".document-viewer").width()/ (1/1.4142))

        $(".document-viewer-outer").width($(".document-viewer-outer").width()/1.2)
        $(".document-viewer-outer").height($(".document-viewer-outer").width()/ (1/1.4142))


        $(".document-viewer-wrapper").width($(".document-wrapper").width()/1.2)
        $(".document-viewer-wrapper").height($(".document-wrapper").width()/ (1/1.4142))
        
        $(".go-to-page input").val();
        $('.go-to-page input').trigger("change");
    });

    $('.wtop-menu li').click(function() {
        $('.wtop-menu li').removeClass('active');
        $(this).addClass('active');
        whiteboard_settings.erase = false;
        $(".upper-canvas").removeClass("show_eraser");
    });

    $('.navbar-menu li').click(function() {
       $(".wtop-menu").hide();
       $('.navbar-menu li').removeClass('active');
       $(this).addClass('active');
       whiteboard_settings.erase = false;
       $(".upper-canvas").removeClass("show_eraser");

   });

    $(document).on('click', '[data-type="image"]', function() {
        $(this).append('<i id="mini-loader" class="fa fa-spinner fa-pulse"></i>');
    });

//sub actions
$("#draw_highlight").click(function() {
  text_settings.highlight=true;
  draw_obj(true);

});

$("#draw_pencil").click(function() {
    text_settings.highlight=false;
    draw_obj(true);
});


function text_reset(){
    itextbold = 'normal';
    itextstyle = 'normal';
    itextdecor= '';
    draw_obj(false);
}
$("#text_bold").click(function() {
    text_reset();
    itextbold = '800';
    var obj = canvas.getActiveObject();
    if(obj){
        obj.fontWeight = itextbold;
        canvas.renderAll();
        send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)
    }

});

$("#text_italic").click(function() {
    text_reset();
    itextstyle = 'italic';
    var obj = canvas.getActiveObject();
    if(obj){
        obj.fontStyle = itextstyle
        canvas.renderAll();
        send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)
    }
});

$("#text_underline").click(function() {
    text_reset();
    itextdecor = 'underline';
    var obj = canvas.getActiveObject();
    if(obj){
        obj.textDecoration = itextdecor
        canvas.renderAll();
        send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)
    }

});

$("#text_strike").click(function() {
    text_reset();
    itextdecor = 'line-through';
    var obj = canvas.getActiveObject();
    if(obj){
        obj.textDecoration = itextdecor
        canvas.renderAll();
        send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)
    }
});
$("#text_heading").click(function() {
    text_reset();
    var obj = canvas.getActiveObject();
    if(obj){
        obj.fontSize = 36;
        obj.setShadow('rgba(0,0,0,0.7) 1px 1px 1px');
        canvas.renderAll();
        send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)
    }
});

$("#text_normal").click(function() {
    text_reset();
    var obj = canvas.getActiveObject();
    if(obj){
        obj.fontWeight = 'normal';
        obj.fontStyle ='normal';
        obj.shadow ='';
        obj.textDecoration = '';
        obj.fontSize = 18;
        canvas.renderAll();
        send_update_text(obj.id, obj.text, obj.fontSize, obj.fontWeight, obj.fontStyle, obj.textDecoration)
    }
});


$("#material-hide").click();

$("#filesearch").keyup(function () {
  var filter = $(this).val();
  $("ul.document-list li").each(function () {
    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
        $(this).hide();
    } else {
      $(this).show();
      $(this).closest(".accordion-body").collapse('show');
  }
});
});

function troubleshooter() {
  canvas.clear();
  $(".document-viewer-wrapper").hide();
  $(".canvas-container").hide();
  $("#whiteboard-embedd").show();
  $("#youtube-embedd").hide();
  $("#whiteboard-embedd").attr("src", "/inclass-troubleshooter");
}
$(".show-tshooter").click(function(){
  troubleshooter();
})


$("#start-togetherjs").click(function() {
  startclass();
});

function extratime() { 
  console.log("time up");
  class_settings.bad_end=false;
}

$('#preclass').on('hidden.bs.modal', function () {
 $("ul.lang-ul").hide();
})

$(window).load(function(){
  $("#prestartloader").hide();
  $("#startloader").hide();
  $('#main-pane').addClass("opacity-in");

    $('#preclass').modal('show');
    $("#wait-teacher").hide();
    $("#startbuttons").delay(400).fadeIn();


});