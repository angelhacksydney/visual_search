from django import forms

class SearchForm(forms.Form):
    searchword = forms.CharField(label='Search', max_length=255)