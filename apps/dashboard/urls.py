
from django.conf.urls import include, url
from dashboard.views import ExploreView, QuizView, SearchView, CreateView

urlpatterns = [
    url(r"^$", ExploreView, name="explore"),
    url(r"^quiz/$", QuizView.as_view(), name="quiz"),
    url(r"^create/", CreateView.as_view(), name="create"),
    url(r"^search/$", SearchView, name="search"),
]
