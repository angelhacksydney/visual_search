import requests
import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView
from .forms import SearchForm

def searchword(word):
    mains = word.replace(" ", "%20")
    main_resp = requests.post('https://api.havenondemand.com/1/api/sync/extractentities/v2?text=' + mains +
                              '&entity_type=people_eng&entity_type=places_eng&entity_type=teams_eng&show_alternatives=false&apikey=61aa8409-3252-482b-be7d-96282d115919')
    main = main_resp.content

    similar_resp = requests.post('https://api.havenondemand.com/1/api/sync/findsimilar/v1?index_reference=http://en.wikipedia.org/wiki/' +
                                 mains + '&highlight=sentences&indexes=wiki_eng&total_results=false&apikey=61aa8409-3252-482b-be7d-96282d115919')
    similar = similar_resp.content
    return {'main': main, 'similar': similar}

def ExploreView(request):

    return render(request, 'dashboard.html', searchword("barack obama") )


def SearchView(request):
	data = None
	jsond = {}
	form = SearchForm()
	if request.method == 'POST':
		form = SearchForm(request.POST)
		if form.is_valid():
			data = form.cleaned_data
			json_data = searchword(data.get('searchword'))
			jsond = json.dumps(json_data)
	return HttpResponse(jsond, content_type="application/json")


class CreateView(TemplateView):
    template_name = "create.html"


class QuizView(TemplateView):
    template_name = "quiz.html"
